Summary: simul benchmark
name: simul
Version: 1.14
Release: 1%{?dist}
License: GPL
Group: Utilities/Benchmarking
URL: http://downloads.whamcloud.com/public/tools/
Source: %{name}-%{version}.tar.gz
BuildRoot: /tmp/%{name}-buildroot

%description
simul benchmark

%prep
%setup -q

%build
%{?_mpich_load}
make
%{?_mpich_unload}

%install
%{?_mpich_load}
%if 0%{?rhel} != 8 && 0%{?rhel} != 9
MPI_BIN=%{_bindir}
%endif
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT${MPI_BIN}
install -s -m 755 simul $RPM_BUILD_ROOT${MPI_BIN}
%{?_mpich_unload}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%if 0%{?rhel} == 8 || 0%{?rhel} == 9
%{_libdir}/mpich/bin/simul
%else
%{_bindir}/simul
%endif

%changelog
