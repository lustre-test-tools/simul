#!/bin/bash

cd simul-*
checkinstall --install=no --pkgname=simul -y -D sh -c "make && cp simul /usr/local/bin"
